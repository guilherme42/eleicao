/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author Seink
 */
public class Voto {

    public Voto(Eleicao eleicao, Eleitor eleitor, Cargo cargo, Candidato candidato) {
        this.eleicao = eleicao;
        this.eleitor = eleitor;
        this.cargo = cargo;
        this.candidato = candidato;
    }
    
    public Eleicao eleicao;
    public Eleitor eleitor;
    public Cargo cargo;
    public Candidato candidato;
    public String protocolo;

    @Override
    public String toString() {
        return cargo.nome+": "+candidato.nome;
    }
}
