/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.File;

/**
 *
 * @author Seink
 */
public class Candidato {

    public Candidato(String nome, Cargo cargo, File file) {
        this.nome = nome;
        this.cargo = cargo;
        this.file = file;
    }
    
    public String nome;
    public Cargo cargo;
    public File file;
}
