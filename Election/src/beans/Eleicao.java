/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.sql.Timestamp;

/**
 *
 * @author Seink
 */
public class Eleicao {

    public Eleicao(String nome, Timestamp dataInicio, Timestamp dataFim) {
        this.nome = nome;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
    }
    
    public String nome;
    public Timestamp dataInicio;
    public Timestamp dataFim;
}
