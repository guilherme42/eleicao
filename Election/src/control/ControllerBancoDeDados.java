package control;


import beans.*;
import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Seink
 */
public class ControllerBancoDeDados {
    public static List<Eleicao> eleicoes = new ArrayList<>();
    public static List<Cargo> cargos = new ArrayList<>();
    public static List<Candidato> candidatos = new ArrayList<>();
    public static List<Eleitor> eleitores = new ArrayList<>();
    public static Eleitor eleitorAtual;
    public static List<Voto> votos = new ArrayList<>();
    
    public static Eleicao eleicaoAtiva(){
        Timestamp now = new Timestamp(new Date().getTime());
        for(int i = 0;i<eleicoes.size();i++){
            Eleicao x = eleicoes.get(i);
            if(x.dataInicio.before(now)&&x.dataFim.after(now)){
                return x;
            }
        }
        return null;
    }
    
    public static Eleicao ultimaEleicaoFinalizada(){
        
        Timestamp now = new Timestamp(new Date().getTime());
        Timestamp lastElection = null;
        Eleicao escolhida = null;
        for(int i = 0;i<eleicoes.size();i++){
            Eleicao x = eleicoes.get(i);
            if(x.dataFim.before(now)){
                if(lastElection==null){
                    escolhida = x;
                    lastElection = x.dataFim;
                }else{
                    if(lastElection.before(x.dataFim)){
                        escolhida = x;
                        lastElection = x.dataFim;
                    }
                }
            }
        }
        return escolhida;
    }

    public static Cargo getCargoPorNome(String selectedItem) {
        for(int i = 0; i< cargos.size();i++){
            if(cargos.get(i).nome.equals(selectedItem)){
                return cargos.get(i);
            }
        }
        return null;
    }

    public static List<Candidato> getCandidatosPorCargo(Cargo cargo) {
        List<Candidato> filtrados = new ArrayList<>();
        for(int i = 0; i< candidatos.size(); i++){
            if(candidatos.get(i).cargo==cargo){
                filtrados.add(candidatos.get(i));
            }
        }
        return filtrados;
    }

    public static Candidato getCandidatoPorNome(String nome) {

        for(int i = 0; i< candidatos.size();i++){
            if(candidatos.get(i).nome.equals(nome)){
                return candidatos.get(i);
            }
        }
        return null;    
    }

    public static String adicionaVotos(List<Voto> novosvotos) {
        Random r = new Random();
        
        String novoProtocolo = completaComZero(r.nextInt(9999)+"")+"-"+completaComZero(r.nextInt(9999)+"")+"-"+completaComZero(r.nextInt(9999)+"")+"-"+completaComZero(r.nextInt(9999)+"");
        for(int i = 0; novosvotos.size()>i;i++){
            novosvotos.get(i).protocolo = novoProtocolo;
        }
        votos.addAll(novosvotos);
        return novoProtocolo;
    }
    
    public static String resultadoParcial(){
        Eleicao atual = eleicaoAtiva();
        int count = 0;
        for(int i = 0; i< votos.size(); i++){
            if(votos.get(i).eleicao==atual){
                count++;
            }
        }
        return count+" votos realizados.";
    }
    
    public static List<String> resultadoFinal(){
        Eleicao atual = ultimaEleicaoFinalizada();
        LinkedList<String> results = new LinkedList<>();
        Score[] contagem = new Score[candidatos.size()];
        for (int i = 0; i<contagem.length; i++) {
            contagem[i] = new Score();
            contagem[i].candidato = candidatos.get(i);
        }
        for(int i = 0; i< votos.size(); i++){
            if(votos.get(i).eleicao==atual){
                contagem[candidatos.indexOf(votos.get(i).candidato)].votos += 1;
            }
        }
        contagem = organizaVotos(contagem);
        
        for(int i = 0; i< contagem.length; i++){
            results.addLast(contagem[i].candidato.cargo+": "+contagem[i].candidato.nome+" - "+contagem[i].votos+" votos");
        }
        return results;   
    }
    
    private static String completaComZero(String n){
        int sz = 4-n.length();
        for(int i = 0; i< sz; i++){
            n = "0"+n;
        }
        return n;
    }

    private static Score[] organizaVotos(Score[] contagem) {
        for (int i = 0; i < contagem.length; i++) {//organiza por numero de votos
            for (int j = i+1; j < contagem.length; j++) {
                if(contagem[i].votos<contagem[j].votos){
                    Score tmp = contagem[i];
                    contagem[i] = contagem[j];
                    contagem[j] = tmp;
                }
            }
        }
        for (int i = 0; i < contagem.length; i++) {//organiza por cargo
            for (int j = i+1; j < contagem.length; j++) {
                if(cargos.indexOf(contagem[i].candidato.cargo)>cargos.indexOf(contagem[j].candidato.cargo)){
                    Score tmp = contagem[i];
                    contagem[i] = contagem[j];
                    contagem[j] = tmp;
                }
            }
        }
        return contagem;
    }
}
