package forms.components;


import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Seink
 */
public class JImagePanel extends JPanel{

    private File source;
    private BufferedImage image;

    public JImagePanel() {
    }

    public void loadImage(File imageFile){
        try {
            source = imageFile;
            image = ImageIO.read(imageFile);
            this.revalidate();
            this.validate();
            this.updateUI();
        } catch (Exception ex) {
            source = null;
            image = null;
        }
    }
    
    public File getSource(){
        return source;
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if(image!=null){
            g.drawImage(image, 0, 0, this); // see javadoc for more info on the parameters            
        }
    }

}