package forms.components;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Seink
 */
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.Action;

public abstract class ActionListener implements Action{
    List<PropertyChangeListener> listeners = new ArrayList<>();
            HashMap<String,Object> values = new HashMap<>();
            boolean enable = true;
            
            @Override
            public Object getValue(String key) {
                return values.get(key);
            }

            @Override
            public void putValue(String key, Object value) {
                values.put(key, value);
            }
            
            @Override
            public void setEnabled(boolean b) {
                enable = b;
            }

            @Override
            public boolean isEnabled() {
                return enable;
            }
            
            @Override
            public void addPropertyChangeListener(PropertyChangeListener listener) {
                listeners.add(listener);
            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener listener) {
                listeners.remove(listener);
            }

}
